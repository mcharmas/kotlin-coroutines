package com.training.charmas.app.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.training.charmas.app.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun showProgress(progressVisible: Boolean) {
        if (progressVisible) {
            progressView.visibility = View.VISIBLE
            helloView.visibility = View.GONE
        } else {
            progressView.visibility = View.GONE
            helloView.visibility = View.VISIBLE
        }
    }
}
